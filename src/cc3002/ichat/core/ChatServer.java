package cc3002.ichat.core;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ChatServer implements Runnable{
	private int port;
	
	public ChatServer(int port){
		this.port= port;
	}
	
	@Override
	public void run() {
		try {
			ServerSocket server = new ServerSocket (port);
			while (true) {
				Socket client = server.accept ();
				System.out.println ("Accepted from " + client.getInetAddress ());
				ChatHandler c = new ChatHandler (client);
				c.start ();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main (String args[]) throws IOException {
		int port= Integer.parseInt (args[0]);
	    new ChatServer (port).run();
	  }
}
