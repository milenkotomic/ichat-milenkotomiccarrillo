package cc3002.ichat.client;

import java.util.ArrayList;
import java.util.Arrays;

public class HashtagLabelDecorator extends MessageDecorator {

	public HashtagLabelDecorator(Message message) {
		super(message);
	}
	
	@Override
	public String change(String s){
		ArrayList<String> message = new ArrayList<String>(Arrays.asList(s.split(" ")));
		String out="";
		for (String m : message){
			if (m.compareTo("")!=0) 
				if (m.substring(0,1).compareTo("#")==0 || m.substring(0,1).compareTo("@")==0)
				m="<em>"+m+"</em>";
			out+=m+" ";
		}
		return decoratedMessage.change(out);
	}

}
