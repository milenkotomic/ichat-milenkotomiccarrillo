package cc3002.ichat.client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.*;

/**
 * Clase que establece las componenetes que tendra la interfaz grafica.
 * @author Milenko
 *
 */
public class UIItem extends JPanel{

	private JLabel username;
	private JComponent component;
	
	/**
	 * Constructor de la clase.
	 * @param username Nombre del usuario
	 * @param component JComponent
	 */
	public UIItem(String username,JComponent component){
		this.username=new JLabel(username);
		this.component=component;
		setLayout(new BorderLayout());
		add(this.username,BorderLayout.WEST);
		add(this.component,BorderLayout.CENTER);
		//setBackground(Color.WHITE);
		setOpaque(false);
	}
}
