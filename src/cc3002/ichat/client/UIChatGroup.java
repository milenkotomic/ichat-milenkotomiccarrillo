package cc3002.ichat.client;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JTextArea;

import cc3002.ichat.core.*;


/**
 * Clase que muestra la Interfaz grafica, ademas de actualizarla. 
 * @author Milenko
 *
 */
public class UIChatGroup extends UIFrame implements Observer{
	private ChatClient client;
	private Message goMessage=new NullDecorator("");
	private Message backMessage=new NullDecorator("");
	
	/**
	 * Constructor de la clase, crea el objeto encargado de modificar los mensajes, para mostrarlo
	 * de acuerdo a las opciones activadas por el usuario.
	 * @param client Nombre de usuario, se muestra en el titulo de la ventana.
	 * @param Args Arreglo que contiene las opciones activadas por el usuario.
	 */
	public UIChatGroup(ChatClient client, ArrayList<String> Args){
		super(client.username());
		client.addObserver(this);
		this.client=client;	
		FactoryMessage proxy = new FactoryMessage();
		this.goMessage=proxy.sendMessage(client.username(), Args);
		this.backMessage=proxy.reciveMessage(client.username(), Args);
		
	}
	/**
	 * Este metodo se llama cada ves que alguien manda un mensaje al server
	 * @param o es un objeto ChatClient que recibio el mensaje del server
	 * @param arg es el mensaje en si
	 * 
	 * arg es de tipo String con el siguiente formato 
	 * username - message
	 */
	@Override
	public void update(Observable o, Object arg) {
		String message=(String) arg;
		int index=message.indexOf("-");
		String username= message.substring(0,index+1)+" ";
		message=message.substring(index+1,message.length());
		//agregando el mensaje a la ventana
		addHtmlMessage(username, this.backMessage.change(message));
		//actualizando la ventana
		updateUI();
	}
	/**
	 * Es metodo se invoka cuando alguien presiona el boton enviar
	 * @param text el el objecto que tiene dentro el texto escrito por el usuario
	 */
	@Override
	public void send(JTextArea text) {
		String message=text.getText();
		message=this.goMessage.change(message);			
		client.sendMessage(message);
		super.send(text);
	}
	/**
	 * Este metodo se llama cuando alguien cierra la ventana
	 */
	@Override
	public void close() {
		// cerrando la sesion con el server
		client.closeSession();
		super.close();
	}
	
	public static void main(String[] args) throws UnknownHostException, IOException {
		String host=args[0];
		int port=Integer.parseInt(args[1]);
		String username=args[2];
		ArrayList<String> Args=new ArrayList<String>();
		if (args.length>3){
			for (int i=3; i<args.length; i++)
				Args.add(args[i]);
		}
		Socket socket = new Socket (host, port);
		ChatClient  client=new ChatClient (username,socket.getInputStream (), socket.getOutputStream ());
		UIChatGroup ui=new UIChatGroup(client, Args);
		ui.setVisible(true);
	}
}
