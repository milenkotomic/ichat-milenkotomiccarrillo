package cc3002.ichat.client;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.*;
import javax.swing.text.*;
import javax.swing.text.html.*;

/**
 * Clase que crea los elementos que contiene la ventana
 * @author Milenko
 *
 */
public class UIFrame extends JFrame{
	
	private JPanel messageList;
	private JTextArea textArea;
	private JButton sendButton;
	private JScrollPane scroll;
	
	/**
	 * Constuctor de la clase, recibe el nombre de usuario para ponerlo de titulo.
	 * @param username Nombre de usuario.
	 */
	public UIFrame(String username){
		super(username);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {close();}
		});
		setLayout(new BorderLayout());
		messageList=new JPanel();
		messageList.setLayout(null);
		Dimension size=new Dimension(400,400);
		messageList.setMinimumSize(size);
		messageList.setMinimumSize(size);
		messageList.setPreferredSize(size);
		
		textArea=new JTextArea();
		textArea.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
		sendButton=new JButton("Send");
		sendButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {send(textArea);}
		});
		
		JPanel bottomPanel=new JPanel();
		bottomPanel.setLayout(new BorderLayout());
		bottomPanel.add(textArea,BorderLayout.CENTER);
		bottomPanel.add(sendButton,BorderLayout.EAST);
		scroll=new JScrollPane(messageList);
		scroll.setAutoscrolls(true);
		add(scroll,BorderLayout.CENTER);
		add(bottomPanel,BorderLayout.SOUTH);
		pack();
	}
	
	public void addHtmlMessage(String username,String html){
		JEditorPane editor = new JEditorPane("text/html","<html><body>"+html+"</body></html>");
		editor.setEditable(false);
		addComponent(new UIItem(username,editor));
	}
	
	
	public void addComponent(JComponent component){
		int y=5;
		for(Component c: messageList.getComponents()){
			y+=c.getPreferredSize().getHeight()+2;
		}
		
		component.setBounds(5,y, 380, (int) component.getPreferredSize().getHeight());
		Dimension size=new Dimension(380,(int)(y+component.getPreferredSize().getHeight()+30));
		messageList.setPreferredSize(size);
		messageList.add(component);
		scroll.updateUI();
		JScrollBar vertical = scroll.getVerticalScrollBar();
		vertical.setValue( vertical.getMaximum()+10 );
	}
	
	/**
	 * Cambia el texto por vacio.
	 * @param text Componene donde el usuario escribe el mensaje.
	 */
	public void send(JTextArea text){
		textArea.setText("");
	}
	
	/**
	 * Actualiza la interfaz grafica.
	 */
	public void updateUI(){
		messageList.updateUI();
	}
	
	/**
	 * Cierra la ventana.
	 */
	public void close(){
		setVisible(false);
		System.exit(0);
	}
}
