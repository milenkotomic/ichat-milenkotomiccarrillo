package cc3002.ichat.client;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

public class Tests2 {
	
	@Test
	public void FactoryMessageTest1(){
		FactoryMessage p = new FactoryMessage();
		ArrayList<String> l=new ArrayList<String>();
		Message m = p.reciveMessage("Milenko", l);
		assertTrue(m instanceof NullDecorator);
	}

	@Test
	public void FactoryMessageTest2(){
		FactoryMessage p = new FactoryMessage();
		ArrayList<String> l=new ArrayList<String>();
		l.add("--bwf");
		Message m = p.reciveMessage("Milenko", l);
		assertTrue(m instanceof BadWordDecorator);
	}
	
	@Test
	public void FactoryMessageTest3(){
		FactoryMessage p = new FactoryMessage();
		ArrayList<String> l=new ArrayList<String>();
		l.add("--msc");
		Message m = p.reciveMessage("Milenko", l);
		assertTrue(m instanceof MiniSpellDecorator);
	}
	
	@Test
	public void FactoryMessageTest4(){
		FactoryMessage p = new FactoryMessage();
		ArrayList<String> l=new ArrayList<String>();
		l.add("--msc");
		l.add("--bwf");
		Message m = p.reciveMessage("Milenko", l);
		assertTrue(m instanceof BadWordDecorator);		
	}
	
	@Test
	public void FactoryMessageTest5(){
		FactoryMessage p = new FactoryMessage();
		ArrayList<String> l=new ArrayList<String>();
		l.add("--bwf");
		l.add("--msc");		
		Message m = p.reciveMessage("Milenko", l);
		assertTrue(m instanceof MiniSpellDecorator);		
	}
	
	@Test
	public void FactoryMessageTest6(){
		FactoryMessage p = new FactoryMessage();
		ArrayList<String> l=new ArrayList<String>();
		Message m = p.sendMessage("Milenko", l);
		assertTrue(m instanceof NullDecorator);		
	}
	
	@Test
	public void FactoryMessageTest7(){
		FactoryMessage p = new FactoryMessage();
		ArrayList<String> l=new ArrayList<String>();
		l.add("--ep");
		Message m = p.sendMessage("Milenko", l);
		assertTrue(m instanceof EasyPicDecorator);		
	}
	
	@Test
	public void FactoryMessageTest8(){
		FactoryMessage p = new FactoryMessage();
		ArrayList<String> l=new ArrayList<String>();
		l.add("--hl");
		Message m = p.reciveMessage("Milenko", l);
		assertTrue(m instanceof HashtagLabelDecorator);		
	}
	
	@Test
	public void FactoryMessageTest9(){
		FactoryMessage p = new FactoryMessage();
		ArrayList<String> l=new ArrayList<String>();
		l.add("--bwf");
		l.add("--hl");
		Message m = p.reciveMessage("Milenko", l);
		assertTrue(m instanceof HashtagLabelDecorator);		
	}
	
	@Test
	public void FactoryMessageTest10(){
		FactoryMessage p = new FactoryMessage();
		ArrayList<String> l=new ArrayList<String>();
		l.add("--hl");
		l.add("--msc");		
		Message m = p.reciveMessage("Milenko", l);
		assertTrue(m instanceof MiniSpellDecorator);		
	}
	
	@Test
	public void HashtagLabelTest1(){
		NullDecorator base = new NullDecorator("Milenko");
		HashtagLabelDecorator d=new HashtagLabelDecorator(base);
		String m=d.change("#plas");
		assertEquals(m,"<em>#plas</em> ");
	}
	
	@Test
	public void HashtagLabelTest2(){
		NullDecorator base = new NullDecorator("Milenko");
		HashtagLabelDecorator d=new HashtagLabelDecorator(base);
		String m=d.change("@milenko");
		assertEquals(m,"<em>@milenko</em> ");
	}
	
	@Test
	public void HashtagLabelTest3(){
		NullDecorator base = new NullDecorator("Milenko");
		HashtagLabelDecorator d=new HashtagLabelDecorator(base);
		String m=d.change("@milenko dice #plas para todos");
		assertEquals(m,"<em>@milenko</em> dice <em>#plas</em> para todos ");
	}
	
	
	


}
