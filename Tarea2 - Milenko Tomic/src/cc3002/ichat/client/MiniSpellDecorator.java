package cc3002.ichat.client;

import java.util.ArrayList;
import java.util.Arrays;



/**
 * Clase que modifica el mensaje detectando que palabras de un mensaje no se encentran en el diccionario para
 * mostrarlas en rojo y subrayadas.
 * @author mtomic
 *
 */
public class MiniSpellDecorator extends MessageDecorator{	
	private ArrayList<String> list = new ArrayList<String>(); 
	
	/**
	 * Constructor de la clase, agraga las palabras al diccionario.
	 * @param message Objeto Message.
	 */
	public MiniSpellDecorator(Message message) {
		super(message);
		list.add("metodologias");
		list.add("programacion");
		list.add("datos");
		list.add("pony");
		list.add("pls");
		list.add("bases");
		list.add("mineria");
		list.add("computador");
		list.add("computacion");
		list.add("ciencia");
		list.add("ingenieria");
		
		
		
	}

	
	/**
	 * Metodo que modifica el mensaje, agragando el codigo HTML para mostar las palabras que no estan en el diccionario
	 * en rojo y subrayadas.
	 * @param s Mensaje de entrada que se modificara.
	 * @return Mensaje procesado.
	 */
	@Override
	public String change(String s) {
		ArrayList<String> message = new ArrayList<String>(Arrays.asList(s.split(" ")));
		String out="";
		for (String m : message){
			if (list.contains(m)==false)
				m="<u><font color=\"RED\">"+m+"</font></u>";
			out+=m+" ";
		}
		return decoratedMessage.change(out);

		
	}
}
