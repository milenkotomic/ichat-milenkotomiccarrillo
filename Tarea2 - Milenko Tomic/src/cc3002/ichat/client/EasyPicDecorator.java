package cc3002.ichat.client;

import java.util.ArrayList;
import java.util.Arrays;


/**
 * Clase que dado un mensaje con un formato dado lo modifica agregando el codigo HTML necesario para
 * enviar una imgen.
 * @author mtomic
 *
 */
public class EasyPicDecorator extends MessageDecorator {

	/**
	 * Constructor de la clase.
	 * @param message Objeto Message.
	 */
	public EasyPicDecorator(Message message) {
		super(message);	
	}
	
	/**
	 * Metodo que procesa el mensaje a�adiendo el codigo HTML necesario para enviar la imagen pedida.
	 * @param s Mensaje de entrada.
	 * @return Mensaje procesado.
	 */
	@Override
	public String change(String s){
		ArrayList<String> message = new ArrayList<String>(Arrays.asList(s.split(" ")));
		String out="";
		for (String m : message){
			if (m.compareTo("")!=0 && m.substring(0,1).compareTo("$")==0)
				m="<img src=\"file:images/"+m.substring(1,m.length()-1)+"\" width=100 height=100/>";
			out+=m+" ";
		}
		return decoratedMessage.change(out);
	}
}
