package cc3002.ichat.client;

/**
 * Clase que implementa la interfaz Message, no modifica el mensaje.
 * @author mtomic
 *
 */
public class NullDecorator implements Message {
	private String username;
	
	/**
	 * Constructor de la clase.
	 * @param s Recibe el nombre de usuario del chat.
	 */
	public NullDecorator(String s){
		this.username=s;
	}
	
	/**
	 * Metodo que dado un mensaje lo devuelve sin hacerle nada.
	 * @param s Mensaje de entrada.
	 * @return Mensaje de salida identico al de entrada.
	 */
	@Override
	public String change(String s) {		
		return s;
	}

	/**
	 * Metodo que retorna el nombre de usuario del cliente.
	 */
	@Override
	public String user() {
		return username;
	}
	


}
