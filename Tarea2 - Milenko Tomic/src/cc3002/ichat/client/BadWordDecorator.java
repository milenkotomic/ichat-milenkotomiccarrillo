package cc3002.ichat.client;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Clase que modifica el mensaje censurando las palabras que son consideradas como malas u ofensivas.
 * @author mtomic
 *
 */
public class BadWordDecorator extends MessageDecorator {
	
	private ArrayList<String> list = new ArrayList<String>();

	/**
	 * Constructor de la clase, añada las palabras que se censuraran al diccionario.
	 * @param message Objeto Message.
	 */
	public BadWordDecorator(Message message) {
		super(message);
		list.add("csm");
		list.add("wn");
		list.add("weon");
		list.add("ctm");
		list.add("aweonao");
		
	}
	
	/**
	 * Metodo que modifica el mensaje de entrada, de forma que las palabras del mensaje que estan en el diccionario 
	 * son censuradas.
	 * @param s Mensaje de entrada que se modificara
	 * @return Mensaje procesado.
	 */
	@Override
	public String change (String s){
		ArrayList<String> message = new ArrayList<String>(Arrays.asList(s.split(" ")));
		String out="";
		for (String m : message){
			if (list.contains(m) && m.substring(0, 1)!="$")
				m="#@$!";
			out+=m+" ";
		}
		return decoratedMessage.change(out);
	}
	

}
