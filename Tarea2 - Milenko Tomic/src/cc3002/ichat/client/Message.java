package cc3002.ichat.client;

/**
 * Interfaz que permitira instanciar objetos que modifique el mensaje de acuerdo a lo solicitado.
 * @author mtomic
 *
 */
public interface  Message {	
	
	/**
	 * Metodo que dado un mensaje del chat, lo modifica de acuerdo a lo que necesite.
	 * @param s Mensaje de entrada.
	 * @return Mensaje de salida modificado.
	 */
	public String change(String s);

	/**
	 * Metodo que devuelve el nombre de usuario del chat.
	 * @return Nombre de usuario.
	 */
	public String user(); 
	

}
