package cc3002.ichat.client;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

public class DecoratorTest {
	
	@Test
	public void NullDecoratorTest1() {
		NullDecorator d=new NullDecorator("Milenko");
		assertTrue(d.user()=="Milenko");
	}
	
	@Test
	public void NullDecoratorTest2(){
		NullDecorator d=new NullDecorator("Milenko");
		assertEquals(d.change("Esto es un mensaje"),"Esto es un mensaje");
	}
	
	@Test
	public void NullDecoratorTest3(){
		NullDecorator d=new NullDecorator("Pony");
		assertEquals(d.change("hola wn"), "hola wn");
	}
	
	@Test
	public void NullDecoratorTest4(){
		NullDecorator d=new NullDecorator("LittlePoint");
		assertEquals(d.change("$auxiliar.png$"), "$auxiliar.png$");
	}
	
	@Test
	public void MessageDecoratorTest1(){
		NullDecorator d=new NullDecorator("Milenko");
		MessageDecorator m=new MessageDecorator(d);
		assertEquals(m.user(), "Milenko");		
	}
	
	@Test
	public void MessageDecoratorTest2(){
		NullDecorator d=new NullDecorator("Milenko");
		MessageDecorator m=new MessageDecorator(d);
		assertEquals(m.change("Esto es un mensaje"), "Esto es un mensaje");		
	}
	
	@Test
	public void MessageDecoratorTest3(){
		NullDecorator d=new NullDecorator("Pony");
		MessageDecorator m=new MessageDecorator(d);
		assertEquals(m.change("hola wn"), "hola wn");		
	}
	
	@Test
	public void MessageDecoratorTest4(){
		NullDecorator d=new NullDecorator("LittlePoint");
		MessageDecorator m=new MessageDecorator(d);
		assertEquals(m.change("$auxiliar.png$"), "$auxiliar.png$");		
	}
	
	@Test
	public void MiniSpellDecoratorTest1(){
		NullDecorator base = new NullDecorator("Milenko");
		MiniSpellDecorator d=new MiniSpellDecorator(base);
		assertTrue(d.user()=="Milenko");
	}
	@Test
	public void MiniSpellDecoratorTest2(){
		NullDecorator base = new NullDecorator("Milenko");
		MiniSpellDecorator d=new MiniSpellDecorator(base);
		String m=d.change("metodologias apesta programacion");
		assertEquals(m,"metodologias <u><font color=\"RED\">apesta</font></u> programacion ");
	}
	
	@Test
	public void MiniSpellDecoratorTest3(){
		NullDecorator base = new NullDecorator("Milenko");
		MiniSpellDecorator d=new MiniSpellDecorator(base);
		String m=d.change("hola ke ase");
		assertEquals(m,"<u><font color=\"RED\">hola</font></u> <u><font color=\"RED\">ke</font></u> <u><font color=\"RED\">ase</font></u> ");
	}
	
	@Test
	public void MiniSpellDecoratorTest4(){
		NullDecorator base = new NullDecorator("Pony");
		MiniSpellDecorator d=new MiniSpellDecorator(base);
		String m=d.change("metodologias ciencia computacion");
		assertEquals(m, "metodologias ciencia computacion ");
	}
	
	@Test
	public void BadWordDecoratorTest1(){
		NullDecorator base = new NullDecorator("Milenko");
		MiniSpellDecorator d=new MiniSpellDecorator(base);
		assertTrue(d.user()=="Milenko");
	}
	
	@Test
	public void BadWordDecoratorTest2(){
		NullDecorator base = new NullDecorator("Milenko");
		BadWordDecorator d = new BadWordDecorator(base);
		String m = d.change("$auxiliar.png$");
		assertEquals(m,"$auxiliar.png$ ");		
	}
	
	@Test
	public void BadWordDecoratorTest3(){
		NullDecorator base = new NullDecorator("Pony");
		BadWordDecorator d = new BadWordDecorator(base);
		String m = d.change("hola querido amigo");
		assertEquals(m,"hola querido amigo ");		
	}
	
	@Test
	public void BadWordDecoratorTest4(){
		NullDecorator base = new NullDecorator("Milenko");
		BadWordDecorator d = new BadWordDecorator(base);
		String m = d.change("hola wn csm");
		assertEquals(m,"hola #@$! #@$! ");		
	}
	
	@Test
	public void EasyPicDecoratorTest1(){
		NullDecorator base = new NullDecorator("Milenko");
		EasyPicDecorator d=new EasyPicDecorator(base);
		assertTrue(d.user()=="Milenko");		
	}	
	
	@Test
	public void EasyPicDecoratorTest2(){
		NullDecorator base = new NullDecorator("Milenko");
		EasyPicDecorator d = new EasyPicDecorator(base);
		String m = d.change("$auxiliar.png$");
		assertEquals(m,"<img src=\"file:images/auxiliar.png\" width=100 height=100/> ");
	}
	
	@Test
	public void EasyPicDecoratorTest3(){
		NullDecorator base = new NullDecorator("Pony");
		EasyPicDecorator d = new EasyPicDecorator(base);
		String m = d.change("$ayudante.png$ hola");
		assertEquals(m,"<img src=\"file:images/ayudante.png\" width=100 height=100/> hola ");
	}
	
	@Test
	public void EasyPicDecoratorTest4(){
		NullDecorator base = new NullDecorator("Milenko");
		EasyPicDecorator d = new EasyPicDecorator(base);
		String m = d.change("pls para ti");
		assertEquals(m,"pls para ti ");
	}
	
	@Test
	public void FactoryMessageTest1(){
		FactoryMessage p = new FactoryMessage();
		ArrayList<String> l=new ArrayList<String>();
		Message m = p.reciveMessage("Milenko", l);
		assertTrue(m instanceof NullDecorator);
	}

	@Test
	public void FactoryMessageTest2(){
		FactoryMessage p = new FactoryMessage();
		ArrayList<String> l=new ArrayList<String>();
		l.add("--bwf");
		Message m = p.reciveMessage("Milenko", l);
		assertTrue(m instanceof BadWordDecorator);
	}
	
	@Test
	public void FactoryMessageTest3(){
		FactoryMessage p = new FactoryMessage();
		ArrayList<String> l=new ArrayList<String>();
		l.add("--msc");
		Message m = p.reciveMessage("Milenko", l);
		assertTrue(m instanceof MiniSpellDecorator);
	}
	
	@Test
	public void FactoryMessageTest4(){
		FactoryMessage p = new FactoryMessage();
		ArrayList<String> l=new ArrayList<String>();
		l.add("--msc");
		l.add("--bwf");
		Message m = p.reciveMessage("Milenko", l);
		assertTrue(m instanceof BadWordDecorator);		
	}
	
	@Test
	public void FactoryMessageTest5(){
		FactoryMessage p = new FactoryMessage();
		ArrayList<String> l=new ArrayList<String>();
		l.add("--bwf");
		l.add("--msc");		
		Message m = p.reciveMessage("Milenko", l);
		assertTrue(m instanceof MiniSpellDecorator);		
	}
	
	@Test
	public void FactoryMessageTest6(){
		FactoryMessage p = new FactoryMessage();
		ArrayList<String> l=new ArrayList<String>();
		Message m = p.sendMessage("Milenko", l);
		assertTrue(m instanceof NullDecorator);		
	}
	
	@Test
	public void FactoryMessageTest7(){
		FactoryMessage p = new FactoryMessage();
		ArrayList<String> l=new ArrayList<String>();
		l.add("--ep");
		Message m = p.sendMessage("Milenko", l);
		assertTrue(m instanceof EasyPicDecorator);		
	}

	@Test
	public void MixPlugins1(){
		NullDecorator base = new NullDecorator("Milenko");
		MiniSpellDecorator d2=new MiniSpellDecorator(base);
		BadWordDecorator d1 = new BadWordDecorator(d2);
		String m =d1.change("hola csm");
		assertEquals(m, "<u><font color=\"RED\">hola</font></u> <u><font color=\"RED\">#@$!</font></u> ");		
	}
	
	@Test
	public void MixPlugins2(){
		NullDecorator base = new NullDecorator("Milenko");
		MiniSpellDecorator d2=new MiniSpellDecorator(base);
		BadWordDecorator d1 = new BadWordDecorator(d2);
		String m =d1.change("ciencia pony");
		assertEquals(m, "ciencia pony ");		
	}
	
	/*
	@Test
	public void MixPlugins6(){
		NullDecorator base = new NullDecorator("Milenko");		
		EasyPicDecorator d1 = new EasyPicDecorator(base);
		MiniSpellDecorator d2=new MiniSpellDecorator(d1);
		String m=d2.change("$auxiliar.png$ hola");
		assertEquals(m, "<img src=\"file:images/auxiliar.png\" width=100 height=100/> <u><font color=\"RED\">hola</font></u> ");
		
	}
	*/
	
	
	@Test
	public void MixPlugins3(){
		NullDecorator base = new NullDecorator("Milenko");
		EasyPicDecorator d2 = new EasyPicDecorator(base);
		BadWordDecorator d1=new BadWordDecorator(d2);
		String m=d1.change("$auxiliar.png$ csm");
		assertEquals(m,"<img src=\"file:images/auxiliar.png\" width=100 height=100/> #@$! ");
	}
	
	
	
}
