package cc3002.ichat.client;

import java.util.ArrayList;

/**
 * Clase que permiete crear los objetos decorados dinamicamente.
 * @author Milenko
 *
 */
public class FactoryMessage {

	/**
	 * Constructor de la clase.
	 */
	public FactoryMessage(){	}
	
	/**
	 * Metodo que devuelve un Message con todas las opciones que activo el usuario.  
	 * @param user Nombre de usuario.
	 * @param Args Arreglo de strings con las opciones del usuario.
	 * @return Message encargado de modificar los mensajes.
	 */
	public Message reciveMessage(String user, ArrayList<String> Args){
		Message m1=new NullDecorator(user);
		Message m2=new NullDecorator(user);
		for (String a: Args){		
			if (a.compareTo("--bwf")==0)
				m2=new BadWordDecorator(m1);		
			else if (a.compareTo("--msc")==0)
				m2=new MiniSpellDecorator(m1);
			else if (a.compareTo("--hl")==0)
				m2=new HashtagLabelDecorator(m1);
			m1=m2;
		}
		return m2;
	}
	
	public Message sendMessage(String user, ArrayList<String> Args){
		Message m1=new NullDecorator(user);
		Message m2=new NullDecorator(user);
		for (String a: Args){
			if (a.compareTo("--ep")==0)
				m2=new EasyPicDecorator(m1);
			m1=m2;			
		}
		return m2;
	}
}
