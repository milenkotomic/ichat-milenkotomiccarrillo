package cc3002.ichat.client;


/**
 * Clase que implementa Message, clase padre de las clases que modificaran el mensaje.
 * @author mtomic
 *
 */
public class MessageDecorator implements Message{
	
	    protected Message decoratedMessage;
	    
	    /**
	     * Constructor de la clase, recibe un objeto Message y lo guarda.
	     * @param m Objeto Message
	     */
	    public MessageDecorator(Message m){
	        decoratedMessage = m;    
	    }
	    
	    /**
	     * Metodo que devuelve el nombre de usuario del cliente.
	     * @return Nombre de usuario. 
	     */
	    @Override
	    public String user(){
	    	return decoratedMessage.user();
	    }
	    
	    /**
	     * Metodo que dado un mensaje retorna el mensaje procesado.
	     * @param s Mensaje de entrada
	     * @return Mensaje procesado.
	     */
		@Override
		public String change(String s) {
			return decoratedMessage.change(s);
		}
	   
}
