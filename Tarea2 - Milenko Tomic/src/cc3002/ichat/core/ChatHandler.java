package cc3002.ichat.core;
import java.net.*;
import java.io.*;
import java.util.*;

public class ChatHandler extends Thread {
	
	protected static Vector<ChatHandler> handlers = new Vector<ChatHandler>();
	protected Socket socket;
	protected DataInputStream input;
	protected DataOutputStream output;
	private String username;
	
	public ChatHandler (Socket socket) throws IOException {
		this.socket = socket;
		username="anonymous";
		input = new DataInputStream (new BufferedInputStream (socket.getInputStream ()));
		output = new DataOutputStream (new BufferedOutputStream (socket.getOutputStream ()));
	}

	public void run () {
		try {
			//first message is the user name
			
			handlers.addElement (this);
			username = input.readUTF().toString();	
			System.out.println(username+" has joined");
			broadcast (username + " has joined.");
			while (true) {
				String msg = input.readUTF ();
				/*este es el mensaje que hay que modificar*/
				broadcast (username + " - " + msg);
			}
		} catch (IOException ex) {
			ex.printStackTrace ();
		} finally {
			handlers.removeElement (this);
			broadcast (username + " has left.");
			try {
				socket.close ();
			} catch (IOException ex) {ex.printStackTrace();}
		}
	}

	protected static void broadcast (String message) {
		synchronized (handlers) {
			Enumeration<ChatHandler> e = handlers.elements ();
			while (e.hasMoreElements ()) {
				ChatHandler handler = e.nextElement ();
				try {
					synchronized (handler.output) {
						handler.output.writeUTF (message);
					}
					handler.output.flush ();
				} catch (IOException ex) {
					handler.stop ();
				}
			}
		}
	}
}
